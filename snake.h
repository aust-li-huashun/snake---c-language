#ifndef __SNAKE_H__
#define __SNAKE_H__

#include <stdio.h>
#include <conio.h>
#include <Windows.h>
#include <stdlib.h>

#define WIDTH 32  //宽度
#define HIGHT 32  //高度

//一个身体的对象
struct BODY {
	int X;
	int Y;
};

//定义蛇对象
struct SNAKE {
	struct BODY body[WIDTH * HIGHT]; //蛇身、body[0]--蛇头
	int size;                        //蛇的大小
}snake;  //定义一个全局变量蛇对象

//定义食物对象
struct FOOD {
	int X;
	int Y;
}food;  //一个食物对象

int score = 0;  //分数
int kx = 0;
int ky = 0;
int lastX = 0;   //记录蛇尾坐标
int lastY = 0;

//初始化蛇
void initSnake();

//初始化食物
void initFood();

//初始化界面
void initUI();

void playGame();

void initWall();

void showScore();

#endif // !__SNAKE_H__

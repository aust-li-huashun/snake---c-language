# 贪吃蛇（C语言）

#### 介绍
贪吃蛇的唯一的目标就是长成最长的一条蛇！点击控制键（wasd）控制小蛇走位，吃掉地图上的小圆点，就会变长。小心！蛇头碰到其他蛇就会死亡。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/100032_f57a3c44_5485522.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/095440_abb37e6e_5485522.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/095510_eedca118_5485522.png "屏幕截图.png")

#### 模块描述
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/095249_cb8b7c63_5485522.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0122/095334_849d5e88_5485522.png "屏幕截图.png")

#include "snake.h"

int main()
{
	// 去除光标。
	CONSOLE_CURSOR_INFO cci;
	cci.dwSize = sizeof(cci);
	cci.bVisible = FALSE;  // TRUE :
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cci);
	srand(time(NULL));//播种随机数种子
	initWall();
	initSnake();  //初始化蛇
	initFood();   //初始化食物
	initUI();
	playGame();
	return 0;
}

void initFood() {
	food.X = rand() % WIDTH;  //0-31
	food.Y = rand() % HIGHT;  //0-31
}

void initSnake() {
	snake.size = 2;
	snake.body[0].X = WIDTH / 2;  //蛇头初始化
	snake.body[0].Y = HIGHT / 2;
	snake.body[1].X = WIDTH / 2 - 1;  //蛇一节身体初始化
	snake.body[1].Y = HIGHT / 2;
}

void initUI() {
	//画蛇
	//修改控制台光标
	COORD coord = { 0 };  //COORD结构体 --> 两个成员变量：x，y坐标
	for (int i = 0; i < snake.size; i++) {
		coord.X = snake.body[i].X;
		coord.Y = snake.body[i].Y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
		if (i == 0)
			putchar('@');
		else
			putchar('*');
	}

	coord.X = lastX;
	coord.Y = lastY;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	putchar(' ');

	//画食物
	/*
	注：食物不能出现在蛇的上面
	需要判断
	*/
	coord.X = food.X;
	coord.Y = food.Y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	putchar('$');

}

void playGame() {
	char key = 'd';
	//判断蛇撞墙
	while (snake.body[0].X >= 0 && snake.body[0].X < WIDTH
		&& snake.body[0].Y >= 0 && snake.body[0].Y < HIGHT) {
		initUI();
		//接受用户按键输入
		if (_kbhit()) {
			key = _getch();
			//putchar(key);
		}
		switch (key) {
		case'w':kx = 0; ky = -1; break;
		case'a':kx = -1; ky = 0; break;
		case's':kx = 0; ky = +1; break;
		case'd':kx = +1; ky = 0; break;
		default:
			break;
		}
		//蛇头撞身体
		for (int i = 1; i < snake.size; i++) {
			if (snake.body[0].X == snake.body[i].X
				&& snake.body[0].Y == snake.body[i].Y) {
				return;  //游戏结束
			}
		}
		//蛇头撞食物
		if (snake.body[0].X == food.X && snake.body[0].Y == food.Y) {
			initFood();   //食物消失
			snake.size++; //身体增长
			score++;      //加分
			//加速
		}

		//移动之前记录蛇尾坐标，以便移动后清空
		lastX = snake.body[snake.size - 1].X;
		lastY = snake.body[snake.size - 1].Y;

		//蛇移动，前一节身体给后一节身体赋值
		for (int i = snake.size - 1; i > 0; i--) {
			snake.body[i].X = snake.body[i - 1].X;
			snake.body[i].Y = snake.body[i - 1].Y;
		}
		//蛇头坐标根据用户按键，修改
		snake.body[0].X += kx;
		snake.body[0].Y += ky;

		//清屏
		//system("cls");
		Sleep(400);
	}
	showScore();
}

void initWall() {
	for (int i = 0; i <= HIGHT; i++) {
		for (int j = 0; j <= WIDTH; j++) {
			if (j == WIDTH) {
				printf("#");
			}
			else if (i == HIGHT) {
				printf("#");
			}
			else {
				printf(" ");
			}
		}
		printf("\n");
	}
}

void showScore() {
	//将光标移动到默认位置（不干扰游戏）
	COORD coord;
	coord.X = 0;
	coord.Y = HIGHT + 2;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
	printf("Game Over!!!\n");
	printf("您的成绩为：%d\n", score);
}